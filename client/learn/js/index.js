const Counter = {
  data() {
    return {
      counter: 0,
    };
  },
  mounted() {
    setInterval(() => {
      this.counter++;
    }, 1000);
  },
};

Vue.createApp(Counter).mount("#counter");

const AttributeBinding = {
  data() {
    return {
      message: "You loaded this page on " + new Date().toLocaleString(),
    };
  },
};

Vue.createApp(AttributeBinding).mount("#bind-attribute");

const EventHandling = {
  data() {
    return {
      message: "Hello Vue.js!",
    };
  },
  methods: {
    reverseMessage() {
      this.message = this.message.split("").reverse().join("");
    },
  },
};

Vue.createApp(EventHandling).mount("#event-handling");

const TwoWayBinding = {
  data() {
    return {
      message: "Hello Vue!",
    };
  },
};

Vue.createApp(TwoWayBinding).mount("#two-way-binding");

const ConditionalRendering = {
  data() {
    return {
      seen: true,
    };
  },
};

Vue.createApp(ConditionalRendering).mount("#conditional-rendering");

// Vue wraps an observed array's mutation methods so they will also trigger view updates. The wrapped methods are:
// push()
// pop()
// shift()
// unshift()
// splice()
// sort()
// reverse()
const ListRendering = {
  data() {
    return {
      todos: [
        { text: "Learn JavaScript" },
        { text: "Learn Vue" },
        { text: "Build something awesome" },
      ],
    };
  },
};

const listApp = Vue.createApp(ListRendering).mount("#list-rendering");

const TodoList = {
  data() {
    return {
      groceryList: [
        { id: 0, text: "Vegetables" },
        { id: 1, text: "Cheese" },
        { id: 2, text: "Whatever else humans are supposed to eat" },
      ],
    };
  },
};

const app = Vue.createApp(TodoList);

app.component("todo-item", {
  props: ["todo"],
  template: `<li>{{ todo.text }}</li>`,
});

app.mount("#todo-list-app");

const app2 = Vue.createApp({
  data() {
    return { count: 4 };
  },
});

const vm = app2.mount("#app2");

console.log(vm.$data.count); // => 4
console.log(vm.count); // => 4

// Assigning a value to vm.count will also update $data.count
vm.count = 5;
console.log(vm.$data.count); // => 5

// ... and vice-versa
vm.$data.count = 6;
console.log(vm.count); // => 6

const app3 = Vue.createApp({
  data() {
    return { count: 4 };
  },
  methods: {
    increment() {
      // `this` will refer to the component instance
      this.count++;
    },
  },
});

const vm3 = app3.mount("#app3");

console.log(vm3.count); // => 4

vm3.increment();

console.log(vm3.count); // => 5

Vue.createApp({
  data() {
    return {
      author: {
        name: "John Doe",
        books: [
          "Vue 2 - Advanced Guide",
          "Vue 3 - Basic Guide",
          "Vue 4 - The Mystery",
        ],
      },
    };
  },
  // For the end result, the two approaches are indeed exactly the same.
  // However, the difference is that computed properties are cached based on their reactive dependencies.
  // A computed property will only re-evaluate when some of its reactive dependencies have changed.
  // This means as long as author.books has not changed,
  // multiple access to the publishedBooksMessage computed property will immediately return the previously computed result without having to run the function again.
  computed: {
    // a computed getter
    publishedBooksMessage() {
      // `this` points to the vm instance
      return this.author.books.length > 0 ? "Yes" : "No";
    },
  },
  methods: {
    calculateBooksMessage() {
      return this.author.books.length > 0 ? "Yes" : "No";
    },
  },
}).mount("#computed-basics");

const watchExampleVM = Vue.createApp({
  data() {
    return {
      question: "",
      answer: "Questions usually contain a question mark. ;-)",
    };
  },
  watch: {
    // whenever question changes, this function will run
    question(newQuestion, oldQuestion) {
      if (newQuestion.indexOf("?") > -1) {
        this.getAnswer();
      }
    },
  },
  methods: {
    getAnswer() {
      this.answer = "Thinking...";
      axios
        .get("https://yesno.wtf/api")
        .then((response) => {
          this.answer = response.data.answer;
        })
        .catch((error) => {
          this.answer = "Error! Could not reach the API. " + error;
        });
    },
  },
}).mount("#watch-example");

// watch vs computed
const vm4 = Vue.createApp({
  data() {
    return {
      firstName: "Foo",
      lastName: "Bar",
      fullName: "Foo Bar",
    };
  },
  watch: {
    firstName(val) {
      this.fullName = val + " " + this.lastName;
    },
    lastName(val) {
      this.fullName = this.firstName + " " + val;
    },
  },
}).mount("#demo");

const vm5 = Vue.createApp({
  data() {
    return {
      firstName: "Foo",
      lastName: "Bar",
    };
  },
  computed: {
    fullName() {
      return this.firstName + " " + this.lastName;
    },
  },
}).mount("#demo2");

Vue.createApp({
  data() {
    return {
      isActive: true,
      hasError: false,
    };
  },
}).mount("#htmlClasses");

Vue.createApp({
  data() {
    return {
      classObject: {
        active: true,
        "text-danger": false,
      },
    };
  },
}).mount("#htmlClasses2");

Vue.createApp({
  data() {
    return {
      isActive: true,
      error: null,
    };
  },
  computed: {
    classObject() {
      return {
        active: this.isActive && !this.error,
        "text-danger": this.error && this.error.type === "fatal",
      };
    },
  },
}).mount("#htmlClasses3");

const app6 = Vue.createApp({});

app6.component("my-component", {
  template: `
    <p :class="$attrs.class">Hi!</p>
    <span>This is a child component</span>
  `,
});

app6.mount("#app6");

Vue.createApp({
  data() {
    return {
      awesome: true,
    };
  },
}).mount("#conditional");

Vue.createApp({
  data() {
    return {
      ok: true,
    };
  },
}).mount("#conditional2");

const app7 = Vue.createApp({
  data() {
    return {
      newTodoText: "",
      todos: [
        {
          id: 1,
          title: "Do the dishes",
        },
        {
          id: 2,
          title: "Take out the trash",
        },
        {
          id: 3,
          title: "Mow the lawn",
        },
      ],
      nextTodoId: 4,
    };
  },
  methods: {
    addNewTodo() {
      this.todos.push({
        id: this.nextTodoId++,
        title: this.newTodoText,
      });
      this.newTodoText = "";
    },
  },
});

app7.component("todo-item", {
  template: `
    <li>
      {{ title }}
      <button @click="$emit('remove')">Remove</button>
    </li>
  `,
  props: ["title"],
  emits: ["remove"],
});

app7.mount("#todo-list-example");

const app8 = Vue.createApp({});
app8.component("root", {
  template: `<div><child></child><div>part of root</div></div>`,
});
app8.component("child", {
  template: `<div>part of child</div>`,
});
app8.mount("#component");
