const app = Vue.createApp({
  data() {
    return {
      nickname: "",
      nicknameInput: "",
    };
  },
  methods: {
    setNickname() {
      this.nickname = this.nicknameInput;
    },
  },
});

const vm = app.mount("#app");
