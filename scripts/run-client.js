const liveServer = require("live-server");

(async () => {
  liveServer.start({
    open: false,
    port: 3000,
    root: process.argv?.[2] || "client/app",
    file: "index.html",
  });
})();
